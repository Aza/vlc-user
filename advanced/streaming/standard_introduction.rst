###################################################
Introduction to the Standard Output Module
###################################################

The Standard Output module, also known as the ``std`` module, is the basic output end of a stream-out (``sout``) chain. It helps in answering the question "where to send the content?"  

We use the ``access`` parameter of the ``std`` module to specify weather the output stream is to be:

 + Saved locally (``access=file``).
 + Streamed over the network (``access=http``, or ``access=udp``, etc.)


.. _save-file-locally:

***************************
Save the file locally
***************************

To save a file locally, do the following in the ``std`` block:

 + Set ``access=file``.
 + Set ``dst`` as the name with which you want the file to be saved.

Further, in the ``dst`` parameter, you can include the the ``filepath`` along with the ``filename`` in case you wish to save the file in some place other than the current directory.

A simple example of saving a file is the following:

.. code-block ::

   $ vlc sample.mp3 --sout="#std{access=file, dst=foo/sample_file.mp3}"

The above code will simply make a new folder named ``foo`` and save the file with the name ``sample_file.mp3``.

A better use case of using just the ``std`` module is Remuxing. Remuxing is taking the transcoded streams from a file and storing them in a different container (without re-encoding them). A simple example of remuxing an MP3 file into an MKV container is the following:

.. code-block ::

   $ vlc sample.mp3 --sout="#std{access=file, mux=mkv, dst=sample.mkv}"

Refer to :ref:`Remuxing a file <remuxing-a-file>` to understand the process in more detail.

Saving the file locally is much more useful when the ``std`` module is used with another module. For example, suppose we wish to do some changes in a file that requires re-encoding it, like in the following cases: 

 + Changing a property/setting.
 + Adding an affect or a filter.
 + Re-encoding by using different codecs.
 + Combining multiple files, etc.

In these scenarios, we will first perform the required operation and re-encode the streams using the ``transcode`` module, and then we will save the updated file locally with the help of the ``std`` module. 

Refer to Combining Transcode and Standard Output for understanding how the ``transcoding`` and the ``standard output`` modules interact with each other. You can also refer to How to Transcode a file to understand how to transcode and save the output in a file locally.


.. _stream-over-the-network:

***************************
Stream over the network
***************************

VLC supports streaming over the local network using various transport protocols, like:

 + :ref:`Stream over HTTP <stream-over-http>`.
 + :ref:`Stream over UDP <stream-over-udp>`.
 + :ref:`Stream over RTP <stream-over-rtp>`.

Streaming over HTTP and UDP are defined in the ``std`` module, so we can use the ``std`` block to stream over those protocols. However, streaming over RTP is defined in a separate ``rtp`` module. Hence, we can't stream over RTP using the ``std`` module, we will have to use the ``rtp`` module for doing that. 

Further, another important point to remember is that HTTP is a pull (or server) protocol, while UDP and RTP are push (or broadcast) protocols. It is important to remember this because when we are writing the code for sending or receiving the streams: 
 
 + In the case of pull protocol, the stream is published at the IP address of the streaming device.
 + In the case of push protocol, the stream is pushed to the specified IP address(es).

Accordingly, we specify the IP addresses depending upon whether we are streaming over a pull or a push protocol. You can refer to the pages for streaming over the respective protocols to understand the details.   	    

Along with the above mentioned protocols, VLC also supports using the following session protocols to facilitate the streaming:

 + :ref:`Stream using SAP <stream-with-sap>`. 
 + :ref:`Stream using RTSP <stream-with-rtsp>`.

SAP helps is easier discovery on client devices when multicast streaming over UDP or RTP. 

RTSP helps in negotiating the session parameters between the sender and reciever(s), and then uses RTP for streaming the data.